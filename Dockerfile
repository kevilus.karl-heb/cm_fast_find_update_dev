FROM gcr.io/google-appengine/php:latest

ENV DOCUMENT_ROOT /app
ENV NR_INSTALL_KEY 3b9b760f14439578dd5cef3d52cc6990f93c4f4b
ENV NR_INSTALL_SILENT true
ENV NR_INSTALL_PATH /opt/php71/lib/ext.enabled
ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update; apt-get -y install wget sudo vim
RUN wget -O - https://download.newrelic.com/548C16BF.gpg | sudo apt-key add -
RUN echo 'deb http://apt.newrelic.com/debian/ newrelic non-free' | tee /etc/apt/sources.list.d/newrelic.list
RUN apt-get update && apt-get -y install newrelic-php5
RUN newrelic-install install

RUN cp /etc/newrelic/newrelic.cfg.template /etc/newrelic/newrelic.cfg
RUN mv /opt/php71/lib/ext.enabled:/opt/php71/lib/conf.d/newrelic.ini /opt/php71/lib/ext.enabled/newrelic.ini
