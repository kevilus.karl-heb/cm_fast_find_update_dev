<?php
function customError($errno, $errstr) {
 if($errno > 100)
  { }
}

function update_fast_es($data,$id){
    $curl = curl_init();
    $url = "https://6e892648dafd40ba82582f3b06f50bca.us-central1.gcp.cloud.es.io:9243/fast_sub_locations/_update/$id";
curl_setopt_array($curl, array(
  CURLOPT_URL => $url,
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "POST",
  CURLOPT_POSTFIELDS =>$data,
  CURLOPT_HTTPHEADER => array(
    "Content-Type: application/json",
    "Authorization: Basic ZWxhc3RpYzpTMXNRVUFTNXlUZGVEcFVMN0JWVVo5d1Y="
  ),
));

$response = curl_exec($curl);

curl_close($curl);
$results = json_decode($response,true);
$explode = explode("_",$results['_id']);
$store = $explode[0];
$sc = $explode[1];
$data = array("store" => $store,
    "sub_commodity_id" => $sc,
    "results" => $results['result']);
$return = json_encode($data);
return($return);

}
function get_fast_es($id){
    $curl = curl_init();
$url = "https://6e892648dafd40ba82582f3b06f50bca.us-central1.gcp.cloud.es.io:9243/fast_sub_locations/_doc/$id";
curl_setopt_array($curl, array(
  CURLOPT_URL => $url,
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "GET",
  CURLOPT_HTTPHEADER => array(
    "Content-Type: application/json",
    "Authorization: Basic ZWxhc3RpYzpTMXNRVUFTNXlUZGVEcFVMN0JWVVo5d1Y="
  ),
));

$response = curl_exec($curl);

curl_close($curl);
$results = json_decode($response,TRUE);
return(json_encode($results['_source']));
}
