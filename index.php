<?php
include("functions.php");

set_error_handler("customError");
$json = file_get_contents('php://input');
$obj = json_decode($json, TRUE);
$body = array();
$changearray = array("aisle","sequence","shelf","sect","area_description");
if(isset($obj['store'])&&(isset($obj['sub_commodity_id']))){
     if ($_SERVER["REQUEST_METHOD"] == "PUT"){
    $id = $obj['store'].'_'.$obj['sub_commodity_id'];
    foreach($obj['change'] as $change=>$value){
        if(in_array($change,$changearray)){
        $body = array_merge($body, array($change => $value));

            } }
        $data = array(
                "doc" => $body);
        $esdata = json_encode($data);
        header('Content-Type: application/json');
        print_r(update_fast_es($esdata,$id));
}elseif ($_SERVER["REQUEST_METHOD"] == "GET"){
    $id = $obj['store'].'_'.$obj['sub_commodity_id'];
    header('Content-Type: application/json');
    print_r(get_fast_es($id));
 } else { die("Invalid Input");}
}



